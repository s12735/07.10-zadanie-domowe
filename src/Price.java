
public class Price {
	private float price;
	
	public Price(){};
	public Price(float price){
		this.price = price;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Price =" + price + "";
	}
	
	
	
}
