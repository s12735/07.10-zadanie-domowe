
public class Product {
	private String name;
	private Price price;
	private Promotion promotion;
	private boolean inPromotion;
	
	public Product(String name, Price price, boolean inPromotion, Promotion promotion){
		this.name = name;
		this.price = price;
		
		if(this.promotion != null){
			this.promotion = promotion;
			float tempPrice = price.getPrice();
			float tempProm = promotion.getPercentOfPromotion();
			this.price = new Price(tempPrice - (tempPrice * tempProm));
		}
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	
	public boolean isInPromotion() {
		return inPromotion;
	}

	public void setInPromotion(boolean inPromotion) {
		this.inPromotion = inPromotion;
	}

	@Override
	public String toString() {
		return "Product [name=" + name + ", price=" + price + ", promotion=" + promotion + "]";
	}
	
	
}
