
public class Promotion {
	private float percentOfPromotion;

	public float getPercentOfPromotion() {
		return percentOfPromotion;
	}

	public void setPercentOfPromotion(float percentOfPromotion) {
		this.percentOfPromotion = percentOfPromotion;
	}

	@Override
	public String toString() {
		return "Promotion [percentOfPromotion=" + percentOfPromotion + "]";
	}
	
	
}
