import java.util.ArrayList;
import java.util.List;

public class Cart {
	private List<Product> listOfProduct;
	private Client client;
	private Price price;

	public Cart(Client client) {
		this.client = client;
		this.price = new Price();
		this.listOfProduct = new ArrayList<>();
	}

	public void addProduct(Product prod) {
		if (prod != null) {
			this.listOfProduct.add(prod);
			float priceOfProduct = prod.getPrice().getPrice();
			float promotionOfClient = this.client.getPromotion().getPercentOfPromotion();
			
			if(client.getPromotion() == null){
				this.price.setPrice(this.price.getPrice() + priceOfProduct);
			}
			else{
				
				this.price.setPrice(this.price.getPrice() + (priceOfProduct - priceOfProduct * promotionOfClient));
			}
		}
	}
}
